# Alive
## Members
 - Rostislav Mardyshkin
 - Artem Granik
 - Timofey Luikshin
 - Pavel Pren (Plodnik)

## High-level Overview Describtion
We create JavaFX application which solves genetical problems and imitates the development of population. It can help people to predict biological characteristics and calculate the probability of having certain characteristics in next generation.

## Functional Details
Desktop application with 2 modes: solving genetical problems and simulation. In both modes you have to provide information about at least 2 specimens.

## Implementaion Details
The main language is Java. The application platform is JavaFX. 
